package com.redhat.route;

import com.redhat.entity.User;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import java.util.List;

public class Route extends RouteBuilder {

    public static final String MEDIA_TYPE_APP_JSON = "application/json";

    @Override
    public void configure() throws Exception {

        rest("/user")
            .bindingMode(RestBindingMode.json)
            .post()
                .type(User.class)
                .outType(User.class)
                .consumes(MEDIA_TYPE_APP_JSON)
                .produces(MEDIA_TYPE_APP_JSON)
                .to("direct:user-post")
            .get()
                .outType(List.class)
                .produces(MEDIA_TYPE_APP_JSON)
                .to("direct:user-get");

        from("direct:user-post")
                .routeId("save-user-route")
                .log("saving users")
                .to("jpa:" + User.class.getName());

        from("direct:user-get")
                .routeId("list-user-route")
                .log("listing users")
                .to("jpa:" + User.class.getName()+ "?query={{query}}");
      
    }
}